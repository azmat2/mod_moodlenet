<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mandatory public API of module
 *
 * @package    moodlenet_url
 * @copyright  2019 Mayel de Borniol  {@link http://mayel.space}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

/**
 * List of features supported in module
 *
 * @param string $feature FEATURE_xx constant for requested feature
 * @return mixed True if module supports feature, false if not, null if doesn't know
 */
function moodlenet_supports($feature) {
    switch ($feature) {
        case FEATURE_MOD_ARCHETYPE:
            return MOD_ARCHETYPE_RESOURCE;
        case FEATURE_GROUPS:
            return false;
        case FEATURE_GROUPINGS:
            return false;
        case FEATURE_MOD_INTRO:
            return false;
        case FEATURE_COMPLETION_TRACKS_VIEWS:
            return false;
        case FEATURE_GRADE_HAS_GRADE:
            return false;
        case FEATURE_GRADE_OUTCOMES:
            return false;
        case FEATURE_BACKUP_MOODLE2:
            return false;
        case FEATURE_SHOW_DESCRIPTION:
            return false;

        default:
            return null;
    }
}

/**
 * Add url instance.
 *
 * @param object $data
 * @param object $mform
 * @return int new url instance id
 */
function moodlenet_add_instance($data, $mform) {


    return false;
}

function curlHeaderCallbackforAttachment($resURL,
        $strHeader) { // to support downloading files that are redirect using the attachment header
    if (preg_match('/^HTTP/i', $strHeader)) {
        header($strHeader);
        header('Content-Disposition: attachment; filename="file-name.zip"');
    }
    return strlen($strHeader);
}

/**
 * Scan directory if antivirus enabled.
 *
 * @param string $dir path of directory
 * @throws  void|string show exception in case antivirus detect infected file.
 */
function scan_folder($dir) {
    // Check if $dir is really a directory.
    if (is_dir($dir)) {
        $files = scandir($dir);
        foreach ($files as $file) {

            // .tar files contain ".", ".." and ".ARCHIVE_INDEX" by default
            if ($file !== '.' && $file !== '..' && $file !== '.ARCHIVE_INDEX') {
                if (is_dir($dir . '/' . $file)) { // Sub dir.
                    scan_folder($dir . '/' . $file);
                } else {
                    // Scan file in antivirus.
                    \core\antivirus\manager::scan_file($dir . "/" . $file, $file, true);

                }
            }
        }
    }
}
