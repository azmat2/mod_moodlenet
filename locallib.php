<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * MoodleNet library
 *
 * @package    mod_moodlenet
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  2019 Mayel de Borniol  {@link http://mayel.space}
 *
 *
 */

require_once($CFG->dirroot . '/mod/moodlenet/vendor/autoload.php');
require_once('./lib.php');

class mod_moodlenet {

    public function search_url($course = 1, $section = 1) {
        $domain = get_config('mod_moodlenet', 'domainurl');
        if ($domain === false) {
            // Default applied if no config found.
            $domain = 'https://app.next.moodle.net';
        }
        $params = array('sesskey' => sesskey(), 'course' => $course, 'section' => $section);
        $moodle_download_url = new moodle_url("/mod/moodlenet/search.php", $params);
        // $params = array_merge($params, ['add'=>'url', 'section'=>1]);
        // $moodle_download_url = new moodle_url("/course/modedit.php", $params);
        $moodle_core_download_url = $moodle_download_url->out(false);
        $domain = $domain . "/search?moodle_core_download_url=" . urlencode($moodle_core_download_url);
        return $domain;
    }

    public function embed_search($course = 1, $section = 1) {
        $html = '';
        $html .= '<iframe style="min-height:600px;" width="100%" height="100%" src="' . $this->search_url($course, $section) .
                '" frameborder="0" allowfullscreen></iframe>';
        return $html;
    }

    public function redirect_POST($url, $fields, $display_after_btn = "") {
        global $OUTPUT;
        $courseid = optional_param('course', false, PARAM_INT);
        $sectionid = optional_param('section', false, PARAM_INT);
        $fields['sesskey'] = sesskey();

        $addresourcetocourse = get_string('addresourcetocourse', 'mod_moodlenet');
        ?>

        <form id="redirect_form" action="<?= $url ?>" method="post">
            <?php
            foreach ($fields as $a => $b) {
                echo '<input type="hidden" name="' . htmlentities($a) . '" value="' . htmlentities($b) . '">';
            }
            // Display content.
            // echo html_writer::tag('div', $display_after_btn);

            echo '<input type="submit" class="btn btn-primary" name="submission_button" style="margin:10px;" value="Continue">';

            // Display cancel button.
            echo "<input type='button' onclick=document.location.href='search.php?course=$courseid&section=$sectionid' class='btn btn-secondary'
                         name='cancel' id='id_cancel' value=" . get_string('cancel') . ">";
            ?>
        </form>

        <script type="text/javascript">
            function submitForm() {
                document.getElementById('redirect_form').submit();
            }

            // Disable if we want to check the content before sending to resource/activity creation.
            // window.onload = submitForm;
        </script>
        <?php

        // echo $display_after_btn;
        // Display page footer.
        echo $OUTPUT->footer();

        exit();
    }

    public function import_resource($url, $resource_name = '', $resource_description, $moodleneturl = '', $backupsize = 0) {

        $sizeinfo = new stdClass();
        $sizeinfo->total = number_format($backupsize / 1000000, 2);

        // echo html_writer::tag('div', get_string('downloadingmsg', 'mod_moodlenet'),
        //        array('class' => 'textinfo'));

        // Try showing a loading message to make the user wait.
        if (ob_get_level()) {
            ob_flush();
        }
        flush();

        return $this->fetch_external($url, $resource_name, $resource_description, $moodleneturl);

    }

    /**
     * Downloads URL, checks the content, and - if a course backup - stores it in the user private files
     *
     * @param int $url
     * @param string $resource_name
     * @return array
     */
    public function fetch_external($url, $resource_name, $resource_description, $moodleneturl) {
        global $CFG, $USER, $OUTPUT, $renderer, $context;

        require_once($CFG->libdir . "/filelib.php");

        try {
            $courseid = required_param('course', PARAM_INT);
            $section = optional_param('section', null, PARAM_INT);

            $backuptempdir = make_request_directory();
            $filename = md5(time() . '-' . $url . '-' . $USER->id . '-' . random_string(20));
            $dl_file_pathname = $backuptempdir . '/' . $filename . ".mbz"; // default to MBZ

            // $this->download_url($url, $dl_file_pathname);

            $dispatcher = new MoodleNetCurlDispatcher([
                            CURLOPT_FOLLOWLOCATION => true,
                            CURLOPT_MAXREDIRS => 20,
                            CURLOPT_CONNECTTIMEOUT => 10,
                            CURLOPT_TIMEOUT => 10,
                            CURLOPT_SSL_VERIFYPEER => false,
                            CURLOPT_SSL_VERIFYHOST => false,
                            CURLOPT_ENCODING => '',
                            CURLOPT_AUTOREFERER => true,
                            CURLOPT_USERAGENT => 'Android',
                            CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4,
                        // CURLOPT_HEADER => true,
                            CURLOPT_FAILONERROR => true, // Fail if the HTTP code returned is greater than or equal to 400
                    ]
            );
            $dispatcher->set_save_path($dl_file_pathname); // path to be used to save binary files

            // Fetch url, analyse the file, get info and save.
            $embed_info = Embed\Embed::create($url, null, $dispatcher);
            $embed_response = $embed_info->getResponse();

            $mimes = [];

            if (file_exists($dl_file_pathname)) {

                // Check file if enabled antivirus and if infected remove file.
                \core\antivirus\manager::scan_file($dl_file_pathname, $filename, true);

                if (function_exists('finfo_file')) {
                    $finfo = finfo_open(FILEINFO_MIME_TYPE); // get file's mime type
                    $mimes[] = finfo_file($finfo, $dl_file_pathname); // get mime type from file
                    finfo_close($finfo);
                } else if (function_exists('mime_content_type')) {
                    // If finfo_file() php function not available, fall back to older mime_content_type()
                    $mimes[] = mime_content_type($dl_file_pathname);
                }

            }


            //$whitelist_exts = [
            //         'pdf',
            //         'rtf', 'docx', 'doc', 'odt', 'ott',
            //         'xls', 'xlsx', 'ods', 'ots', 'csv',
            //         'ppt', 'pps', 'pptx', 'odp', 'otp', 'key',
            //         'odg', 'otg', 'otf', 'odc',
            //         'ogg', 'mp3', 'm4a', 'midi', 'mid', 'wav',
            //         'mp4', 'flv', 'mkv', 'avi',
            //         'gif', 'jpg', 'jpeg', 'png', 'svg', 'webm', 'eps',
            //         'md', 'tex',
            //         'mbz'
            // ];
            $whitelist_exts = explode(";", trim(get_config('mod_moodlenet', 'allowedextensions')));
            $whitelist_exts = array_map('trim', $whitelist_exts);


            // TODO Remove - Getting allowed MIME types from settings.
            // $allowed_mimetypes = explode(";", trim(get_config('mod_moodlenet', 'allowedmimetypes')));

            $whitelisted_as_file_to_attach = false;
            $exts = [];

            foreach ($mimes as $mime) {
                $ext = \core_filetypes::get_file_extension($mime); // turn mime type into a file extension

                if (strpos($mime, 'zip') !== false) {
                    // ZIP files will be treated as MBZ (Moodle course backup)
                    $ext = 'mbz';
                    $mime = 'application/vnd.moodle.backup';
                }
            }

            foreach($exts as $ext){

                 // Checking if the file allowed to attach.
                 if (!in_array($ext, $whitelist_exts)) {

                    $whitelisted_as_file_to_attach = false;
                    break; // do not attach if any possible extension isn't in the whitelist, the resource may still be added, just not as a File resource

                } else {
                    $whitelisted_as_file_to_attach = true;
                }
            }

            // Now that we've been careful with what files to attach, let's also get mime type from HTTP response, in case that provides us other/better information than just looking at the file...
            $mimes[] = $embed_response->getContentType(); // get mime type from http/curl

            if (in_array('text/html', $mimes) || in_array('text/xml', $mimes)) {
                // do not attach webpages
                $whitelisted_as_file_to_attach = false;
                $seems_like_webpage = true;
            }

            $moodle_resource_type = 'url'; // Default to creating a Link/URL resource.
            $filemanageritemid = null;
            $content = null;
            $embed_code = $resource_preview = "";

            $allowobjectembed = empty($CFG->allowobjectembed) ? 0 : 1; // Moodle embed setting needs to be ON

            if($embed_info->code && $allowobjectembed){

                // TODO figure out how to make embeds work despite Moodle security (by default embed and iframe settings are OFF)

                $embed_code = purify_html($embed_code); // remove JS - TODO test with embeds from various sources
            }


            if ($whitelisted_as_file_to_attach === true) { // treat as file

                $fs = get_file_storage();

                $record = new stdClass();
                $record->contextid = context_user::instance($USER->id)->id;
                $record->component = 'user';
                $record->filearea = 'draft';
                $record->itemid = file_get_unused_draft_itemid();
                $record->filename = clean_param(trim(basename($resource_name)) . '.' . $ext, PARAM_FILE);
                $record->filepath = '/';

                // Check course file size limit in course.
                // Ignore mbz file or if the logged in user is Administrator.
                if (!isset($exts['mbz']) && !has_capability('moodle/site:config', $context)) {
                    $areamaxbytes = $GLOBALS["parentcourse"]->maxbytes;
                    $result = $embed_response->getInfo();
                    $filesize = $result["size_download"];
                    if (file_is_draft_area_limit_reached($record->itemid, $areamaxbytes, $filesize)) {
                        throw new file_exception('maxareabytes');
                    }
                }

                $fs->create_file_from_pathname($record, $dl_file_pathname);

                $filemanageritemid = $record->itemid;

                if (isset($exts['mbz'])) { // Dealing with a course backup.
                    echo $OUTPUT->notification(get_string('downloaded', 'mod_moodlenet', $record->filename), 'notifysuccess');

                    // Check for viruses.
                    // Decompress .mbz to .tar if antivirus enabled.
                    // Get installed antivirus plugins.
                    $scanners = \core\antivirus\manager::get_available();
                    if ($scanners) {
                        $p = new PharData($dl_file_pathname);
                        $p->decompress(".tar");

                        // Un-archive from the .tar.
                        $dir = str_replace('.mbz', '', $dl_file_pathname);
                        $phar = new PharData($dir . ".tar");
                        $phar->extractTo($dir);
                        // Scan the folder.
                        scan_folder($dir);
                    }

                    $backuptempdir = make_backup_temp_directory('');
                    $filename = md5(time() . '-' . $USER->id . '-' . random_string(20));
                    $path = $backuptempdir . '/' . $filename;
                    // Move the file to where the restore process will look for it.
                    if (rename($dl_file_pathname, $path)) {
                        $restoreurl =
                                new moodle_url('/backup/restore.php', array('filename' => $filename, 'contextid' => $context->id));
                        $params =
                                array('course' => $courseid, 'section' => $section, 'cancelrestore' => 1, 'filename' => $filename);
                        $cancelurl = new moodle_url("/mod/moodlenet/search.php", $params);
                        $restoreurl_btn = $OUTPUT->continue_button($restoreurl);
                        $cancelurl_btn = $OUTPUT->single_button($cancelurl, get_string('cancel'));
                        echo html_writer::tag('div', $restoreurl_btn . '&nbsp&nbsp' . $cancelurl_btn,
                                array('style' => ' display: inline-flex; vertical-align: baseline;'));
                        echo $OUTPUT->footer();
                        //redirect($restoreurl);
                        die();
                    } else {
                        echo get_string('errormovefile', 'mod_moodlenet');
                    }

                } else {
                    // file other than MBZ, attach as file resource.
                    $moodle_resource_type = 'resource';
                }

            } elseif ($embed_code) { // we have an embed code
                // Preview content.
                $content = $embed_code;
                $resource_preview = get_string('preview_embed', 'mod_moodlenet') . '<p>' . $content;

            } elseif ($seems_like_webpage) { // treat like a webpage

                $configuration = new andreskrey\Readability\Configuration([
                        'fixRelativeURLs' => true,
                        'originalURL' => $url,
                ]);

                $readability = new andreskrey\Readability\Readability($configuration);

                // $html = file_get_contents($dl_file_pathname); // prefer to use from memory rather than filesystem.

                $html = $embed_response->getContent();

                try {
                    $readability->parse($html); // parse HTML to extract contents
                    var_dump($readability);

                    if (strlen(strip_tags($readability)) > 400) {
                        // Use the readable text contents if it seems like a long-form article.
                        $content = purify_html($readability); // strip JS etc
                        $resource_preview = get_string('preview_html', 'mod_moodlenet') . '<p>' . $content;
                    }
                } catch (andreskrey\Readability\ParseException $e) {
                    // echo get_string('errorprocessingwebpage', 'mod_moodlenet', $e->getMessage());
                    // we'll just add it as Link resource instead
                }
                


            }

            if (!empty($content)) {
                $moodle_resource_type = 'page'; // Page resource type for HTML or text contents
            }

            // TODO! delete the downloaded file if no longer needed

            $desc = ($resource_description ? $resource_description : $embed_info->description);

            if(!$content && $embed_code) $desc .= "<hr/>" . $embed_code; // add embed in the description for to enable previewing files (except for Page where its in the content field)

            if ($moodleneturl) {
                $desc .= "<hr/><p>" . get_string('resourcecomesfrom', 'mod_moodlenet') .
                        "<a href='$url' target='_blank'>" . get_string('externalsource', 'mod_moodlenet') .
                        "</a>" . get_string('via', 'mod_moodlenet') .
                        "<a href='$moodleneturl' target='_blank'>" . get_string('moodlenetcollection', 'mod_moodlenet') .
                        "</a>.</p>";
            }

            $params = array('course' => $courseid, 'section' => $section, 'add' => $moodle_resource_type);

            $fields = array('externalurl' => $url, 'name' => $resource_name, 'description' => $desc, 'content' => $content,
                    'files' => $filemanageritemid);

            $moodle_download_url = new moodle_url("/mod/moodlenet/add2course.php", $params);

            // Resource imported notification.
            echo $OUTPUT->notification(get_string('downloaded', 'mod_moodlenet', $resource_name), 'notifysuccess');

            // Display content.
            echo html_writer::tag('div', $resource_preview);

            $this->redirect_POST($moodle_download_url, $fields, $resource_preview);

        } catch (Embed\Exceptions\InvalidUrlException $e) {
            echo get_string('errorfetchingresource', 'mod_moodlenet', $e->getMessage());
        } catch (\Throwable $e) { // For PHP 7
            echo get_string('error', 'mod_moodlenet', $e->getMessage());
        } catch (\Exception $e) { // For PHP 5
            echo get_string('error', 'mod_moodlenet', $e->getMessage());
        }
    }

    /**
     * Downloads file - deprecated
     *
     * @param int $url URL of the course on moodle.net
     * @param string $dl_file_pathname local path (in tempdir) to save the downloaded backup to.
     */
    public static function download_url($url, $dl_file_pathname) {
        $fp = fopen($dl_file_pathname, 'w');

        // var_dump($url, $dl_file_pathname);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_exec($ch);
        curl_close($ch);
        fclose($fp);
    }

}

class MoodleNetCurlDispatcher extends Embed\Http\CurlDispatcher {
    public function set_save_path($dl_file_pathname) {
        $this->save_path = $dl_file_pathname;
    }

    protected function exec(Embed\Http\Url $url, array $options) {
        $connection = curl_init((string) $url);

        // $options[CURLOPT_HEADERFUNCTION] = 'curlHeaderCallbackforAttachment';

        curl_setopt_array($connection, $options);

        $curl = new Embed\Http\CurlResult($connection);

        // Get only text responses.
        $curl->onHeader(function($name, $value, $data) {
            if ($name === 'content-type') {
                $data->isBinary = !preg_match('/(text|html|json)/', strtolower($value));
            }
        });

        // $curl->onBody(function ($string, stdClass $data) {
        //     return empty($data->isBinary); // stop if not text/html/json
        // });

        curl_exec($connection);

        $result = $curl->getResult();

        // var_dump('data', $result);
        if ($this->save_path) { //  && $result['data']->isBinary
            file_put_contents($this->save_path, $result['content']); // save fetched file
        }

        curl_close($connection);

        return $this->responses[] = new Embed\Http\Response(
                $url,
                Embed\Http\Url::create($result['url']),
                $result['statusCode'],
                $result['contentType'],
                $result['content'],
                $result['headers'],
                $result['info']
        );
    }

}
