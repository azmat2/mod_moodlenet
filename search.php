<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Controller for various actions of the block.
 *
 * This page display the MoodleNet search form.
 * It also handles downloading a resource to be imported.
 *
 * @package    mod_moodlenet
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  2019 Mayel de Borniol  {@link http://mayel.space}
 */

require('../../config.php');
require_once($CFG->dirroot . '/mod/moodlenet/locallib.php');

require_login();
$courseid = required_param('course', PARAM_INT); //if no courseid is given
$section = optional_param('section', null, PARAM_INT);
$parentcourse = get_course($courseid);

$context = context_course::instance($courseid);
$PAGE->set_course($parentcourse);
$PAGE->set_url('/mod/moodlenet/search.php');
$PAGE->set_heading($SITE->fullname);
$PAGE->set_pagelayout('incourse');
$PAGE->set_title(get_string('searchmoodlenet', 'mod_moodlenet'));
$PAGE->navbar->add(get_string('searchmoodlenet', 'mod_moodlenet'));

$search = optional_param('search', null, PARAM_TEXT);

// If no capability to search course, display an error message.
$usercandownload = true;
// require_capability('moodle/community:add', $context);
// $usercandownload = has_capability('moodle/community:download', $context);
// Allow only course teacher or manager can access search page.
if ($context->contextlevel == CONTEXT_COURSE && !has_capability('mod/moodlenet:allowsearch', $context)) {
    throw new required_capability_exception($context, 'mod/moodlenet:allowsearch', 'nopermissions', '');
}

$renderer = $PAGE->get_renderer('mod_moodlenet');

$moodlenet = new mod_moodlenet();

// Delete temp file when cancel restore.
$cancelrestore = optional_param('cancelrestore', false, PARAM_INT);
if ($usercandownload and $cancelrestore and confirm_sesskey()) {
    $filename = optional_param('filename', '', PARAM_ALPHANUMEXT);
    //delete temp file
    $backuptempdir = make_backup_temp_directory('');
    $nonmbz = file_exists($backuptempdir . '/' . $filename);
    if ($nonmbz) {
        unlink($backuptempdir . '/' . $filename);

    } else {
        unlink($backuptempdir . '/' . $filename . ".mbz");
    }
}

/// Download & add an external resource
$downloadurl = optional_param('sourceurl', '', PARAM_TEXT);
$moodleneturl = optional_param('moodleneturl', '', PARAM_TEXT);
$resource_name = optional_param('name', '', PARAM_TEXT);
$resource_description = optional_param('description', '', PARAM_TEXT);
$backupsize = optional_param('backupsize', 0, PARAM_INT);
if ($usercandownload and !empty($downloadurl)) {
    $PAGE->set_title(get_string('import_resource', 'mod_moodlenet'));
    // OUTPUT: process import.
    echo $OUTPUT->header();
    // echo $OUTPUT->heading(get_string('downloadingresource', 'mod_moodlenet'), 3, 'main');

    echo $OUTPUT->heading_with_help(get_string('import_resource', 'mod_moodlenet'), 'import_resource', 'mod_moodlenet');

    // if(!confirm_sesskey()) die('Session permission invalid');

    $import = $moodlenet->import_resource($downloadurl, $resource_name, $resource_description, $moodleneturl, $backupsize);

    // if($import) echo $renderer->restore_confirmation_box($import, $context);

    echo $OUTPUT->footer();
    die();
}

// Fallback to default action for this page - search.

// OUTPUT.
echo $OUTPUT->header();
echo $OUTPUT->heading_with_help(get_string('import_resources', 'mod_moodlenet'), 'import_resources', 'mod_moodlenet');

if (!empty($errormessage)) {
    echo $errormessage;
}

echo $moodlenet->embed_search($courseid, $section);

echo $OUTPUT->footer();
